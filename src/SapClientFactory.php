<?php

namespace Pavlitom\SapClient;

class SapClientFactory
{
	/**
	 * @param array<string, mixed> $parameters
	 * @return SapClient
	 */
	public function create(array $parameters = [])
	{
		return new SapClient($parameters);
	}

}