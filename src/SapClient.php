<?php
namespace Pavlitom\SapClient;

use DateTime;
use Exception;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerInterface;
use SoapClient;
use SoapFault;

class SapClient implements LoggerAwareInterface, SapClientInterface
{
	/** @var SoapClient|null */
	private $client;

	/** @var LoggerInterface|null */
	private $logger;

	/** @var array<string, mixed> */
	private $log = [];

	/** @var array */
	private $parameters;

	/** @var string|null */
	private $wsdl;

	/**
	 * SapClient constructor.
	 * @param array<mixed, mixed> $parameters
	 */
	public function __construct(array $parameters = [])
	{
		$parameters['trace'] = true;
		$this->parameters = $parameters;
	}

	/**
	 * @return SoapClient|null
	 * @throws SapClientException
	 */
	private function getClient()
	{
		try {
			if (!$this->client) {
				$this->client = new SoapClient($this->wsdl, $this->parameters);
			}
		} catch (SoapFault $fault) {
			if ($this->logger) {
				$this->logger->error($fault->getMessage());
			}

			throw new SapClientException($fault->getMessage());
		}

		return $this->client;
	}

	/** {@inheritDoc} */
	public function setWsdl($wsdl)
	{
		$this->wsdl = $wsdl;
		return $this;
	}

	/** {@inheritDoc} */
	public function call($functionName, $arguments = [])
	{
		$this->log['function_name'] = $functionName;
		$this->log['request_time'] = (new DateTime())->format('Y-m-d H:i:s');
		$this->log['remote_addr'] = filter_input(INPUT_SERVER, "REMOTE_ADDR");

		try {
			$response = $this->getClient()->__soapCall($functionName, [
				$functionName => $arguments,
			]);
			$this->log['failed'] = false;
		} catch (Exception $e) {
			$this->log['failed'] = true;
			$this->log['failed_message'] = $e->getMessage();
			$response = null;
		}

		$this->log['response_time'] = (new DateTime())->format('Y-m-d H:i:s');
		$this->log['request'] = $this->getClient()->__getLastRequest();
		$this->log['request_headers'] = $this->getClient()->__getLastRequestHeaders();
		$this->log['response'] = $this->getClient()->__getLastResponse();
		$this->log['response_headers'] = $this->getClient()->__getLastResponseHeaders();

		if (!$this->logger) {
			return $response;
		}

		if ($this->log['failed']) {
			$this->logger->error($this->log['failed_message'], $this->log);
		} else {
			$this->logger->info('Success.', $this->log);
		}

		return $response;
	}

	/** {@inheritDoc} */
	public function setLogger(LoggerInterface $logger)
	{
		$this->logger = $logger;
	}
}
