<?php
namespace Pavlitom\SapClient;

interface SapClientInterface
{
	/**
	 * @param string $functionName
	 * @param array<mixed, mixed> $arguments
	 * @return mixed
	 * @throws SapClientException
	 */
	public function call($functionName, $arguments = []);

	/**
	 * @param string $wsdl
	 * @return SapClient
	 */
	public function setWsdl($wsdl);
}